# C/C++ GNU ARM Toolchain
set(TOOLCHAIN ${CMAKE_SOURCE_DIR}/toolchain)
set(CMAKE_LINKER ${TOOLCHAIN}/bin/arm-none-eabi-ld)
set(CMAKE_C_FLAGS "-mcpu=arm926ej-s \
    -Xlinker \
    --gc-sections \
    -nostartfiles \
    -T ${CMAKE_SOURCE_DIR}/packages/firmware/lib/at91/linker-scripts/sdram.lds \
    --specs=nano.specs \
    -lc -u _printf_float -u _scanf_float"
)
set(CMAKE_C_COMPILER ${TOOLCHAIN}/bin/arm-none-eabi-gcc)
set(CMAKE_ASM_COMPILER ${TOOLCHAIN}/bin/arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN}/bin/arm-none-eabi-g++)
set(CMAKE_SYSROOT ${TOOLCHAIN}/arm-none-eabi)
set(CMAKE_FIND_ROOT_PATH ${TOOLCHAIN}/arm-none-eabi/bin)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
