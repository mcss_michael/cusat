#include <cusat-hardware/uart.h>

#ifdef at91sam9g20

#define AT91C_US_USMODE_NORMAL (0x0) // (USART) Normal
#define AT91C_US_USMODE_HWHSH (0x2)  // (USART) Hardware Handshaking

typedef enum _UARTbus
{
    bus0_uart = 0,
    bus2_uart = 1,
    UART_BUS_COUNT = 2
} UARTbus;

/*!
 * Defines which type of bus is used for UART2.
 * The UART2 carries a configurable transceiver chip that can support a variety of UART bus types.
 * @note This affects the maximum baud-rate value accepted by the driver for UART2.
 */
typedef enum _UART2busType
{
    rs232_uart = 0,
    rs422_noTermination_uart = 1,
    rs422_withTermination_uart = 2
} UART2busType;

typedef struct _UARTconfig
{
    /*! Value to be written to the UART mode register.
	 *  For UART0, use only normal (AT91C_US_USMODE_NORMAL) mode for the lowest 4 bits. \n
	 *
	 *  For UART2, use only amongst:\n
	 *  normal (AT91C_US_USMODE_NORMAL) or \n
	 *  hardware-handshaking (AT91C_US_USMODE_HWHSH) \n
	 *
	 *  Use only MCK for Clock Selection. See AT91SAM9G20 datasheet.
	 */
    unsigned int mode;
    unsigned int baudrate;    //!< Its suggested to use one of the standard RS-232 baudrates.
    unsigned char timeGuard;  //!< Value to be written to the timeguard register. An idle time of (timeGuard*bitPeriod) is added after each byte. See AT91SAM9G20 datasheet.
    UART2busType busType;     //!< The bus-type to be configured for UART2.
    unsigned short rxtimeout; //!< Timeout value for reception in baudrate ticks, so timeout in seconds is value / baudrate. The timeout only starts counting after the first byte of the transfer has been received. If a timeout is specified it affects all read functions (UART_read, UART_writeRead, UART_queueTransfer).
} UARTconfig;

/*!
 * Starts the selected UART peripheral with the given configuration.
 * @note the UART is enabled after this function
 * @param bus The selected UART bus.
 * @param config The configuration used to initialize the UART peripheral.
 * @return -2 if the bus is invalid OR
 * the baudrate is out of range OR
 * other than normal or hardware-handshaking modes is used
 * -1 if initializing FreeRTOS queues or UART related tasks fails.
 */
int UART_start(UARTbus bus, UARTconfig config);

/*!
 * Stops the selected UART peripheral.
 * @param bus The selected UART bus.
 * @return -1 if the selected bus is out of range, 0 on success.
 */
int UART_stop(UARTbus bus);

/*!
 * Performs a blocking write transfer by sleeping the task until the specified transfer is complete.
 * This allows other tasks to execute while the transfer is made using DMA but the calling task
 * is blocked.
 * @param bus The UART bus over which the transfer should be made.
 * @param data Memory location of data to be sent.
 * @param size Number of bytes to be sent.
 * @return -3 if creating a semaphore for the transfer fails,
 * -2 if input parameters are invalid or the driver is not initialized,
 * -1 if adding the transfer to the queue fails,
 * 0 on success.
 * 2 for general errors during the transfer,
 * 4 for a framing error,
 * 8 for a parity error,
 * 12 if both framing and parity errors occur during the transfer,
 * 16 if an overrun error occurs.
 * @note If a negative error is returned, the transfer was never attempted
 * (for example due to an invalid input).
 * If a positive error is returned, an error occurred during the transfer.
 * @note The positive error values correspond to the members of UARTtransferStatus enumerated data type.
 */
int UART_write(UARTbus bus, unsigned char *data, unsigned int size);

/*!
 * Performs a blocking read transfer by sleeping the task until the specified transfer is complete.
 * This allows other tasks to execute while the transfer is made using DMA but the calling task
 * is blocked.
 * @param bus The UART bus over which the transfer should be made.
 * @param data Memory location where the received data should be stored.
 * @param size Number of bytes to be received.
 * @return -3 if creating a semaphore for the transfer fails,
 * -2 if input parameters are invalid or the driver is not initialized,
 * -1 if adding the transfer to the queue fails,
 * 0 on success.
 * 2 for general errors during the transfer,
 * 4 for a framing error,
 * 8 for a parity error,
 * 12 if both framing and parity errors occur during the transfer,
 * 16 if an overrun error occurs.
 * @note If a negative error is returned, the transfer was never attempted
 * (for example due to an invalid input).
 * If a positive error is returned, an error occurred during the transfer.
 * @note The positive error values correspond to the members of UARTtransferStatus enumerated data type.
 */
int UART_read(UARTbus bus, unsigned char *data, unsigned int size);

#endif

void cusat_UART_start()
{
#ifdef at91sam9g20
    UARTconfig config = {
        .mode = AT91C_US_USMODE_NORMAL,
        .baudrate = 9600,
        .timeGuard = 3,
        .busType = rs232_uart,
        .rxtimeout = 0};
    UART_start(bus0_uart, config);
#endif
}

void cusat_UART_write(const void *const data, const size_t length)
{
#ifdef at91sam9g20
    UART_write(bus0_uart, data, length);
#endif
}

void cusat_UART_read(void *const data, const size_t length)
{
#ifdef at91sam9g20
    UART_read(bus0_uart, data, length);
#endif
}

void cusat_UART_stop()
{
#ifdef at91sam9g20
    UART_stop(bus0_uart);
#endif
}